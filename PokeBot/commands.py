#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import asyncio
import discord
import requests
import copy
from datetime import datetime
from bs4 import BeautifulSoup
from .Filter import load_pokemon_section, load_egg_section
from .utils import (get_args, Dicts, update_dicts, is_number,
                    get_pkmn_id, require_and_remove_key, parse_boolean,
                    get_image_url)

log = logging.getLogger('commands')
args = get_args()


async def status(client, bot_number, message):
    await asyncio.sleep(bot_number * 0.1)
    em = discord.Embed(
        description='PokeBot **{}** (of **{}**) standing by.'.format(
            bot_number + 1, len(args.tokens)),
        color=int('0x71cd40', 16)
    )
    await Dicts.bots[bot_number]['out_queue'].put((
        1, Dicts.bots[bot_number]['count'], {
            'destination': message.channel,
            'embed': em,
            'timestamp': datetime.utcnow()
        }
    ))
    Dicts.bots[bot_number]['count'] += 1
    if bot_number == 0:
        await asyncio.sleep(0.1 * int(len(args.tokens)))
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'msg': 'Systems Operational',
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1


async def commands(bot_number, message):
    em = discord.Embed(
        description=Dicts.info_msg,
        color=int('0x71cd40', 16)
    )
    await Dicts.bots[bot_number]['out_queue'].put((
        1, Dicts.bots[bot_number]['count'], {
            'destination': message.channel,
            'embed': em,
            'timestamp': datetime.utcnow()
        }
    ))
    Dicts.bots[bot_number]['count'] += 1


async def dex(bot_number, message):
    pokemon = message.content.lower().split()[1]
    dex_number = get_pkmn_id(pokemon)
    if dex_number is not None:
        site = "https://pokemongo.gamepress.gg/pokemon/{}".format(dex_number)
        page = requests.get(site)
        soup = BeautifulSoup(page.content, 'html.parser')

        rating = soup.find_all(class_="pokemon-rating")
        max_cp = soup.find_all(class_="max-cp-number")
        stats = soup.find_all(class_="stat-text")
        types = soup.find_all(class_=("field field--name-field-pokemon-type " +
                                      "field--type-entity-reference " +
                                      "field--label-hidden field__items"))
        female = soup.find_all(class_="female-percentage")
        male = soup.find_all(class_="male-percentage")

        quick = []
        legacy_quick = []
        for quick_move in soup.find_all(class_=(
                "views-field views-field-field-quick-move")):
            quick.append(quick_move.find(class_=(
                "field field--name-title " +
                "field--type-string field--label-hidden")))
            legacy_quick.append(quick_move.find(class_=(
                "move-info")))

        charge = []
        legacy_charge = []
        for charge_move in soup.find_all(class_=(
                "views-field views-field-field-charge-move")):
            charge.append(charge_move.find(class_=(
                "field field--name-title " +
                "field--type-string field--label-hidden")))
            legacy_charge.append(charge_move.find(class_=(
                "move-info")))

        legacy_moves = []
        for (legacy_quick, legacy_charge) in zip(legacy_quick, legacy_charge):
            try:
                if legacy_quick.get_text() == '* ':
                    legacy_moves.append(' (Legacy)')
                else:
                    try:
                        if legacy_charge.get_text() == '* ':
                            legacy_moves.append(' (Legacy)')
                        else:
                            legacy_moves.append('')
                    except AttributeError:
                        legacy_moves.append('')
            except AttributeError:
                try:
                    if legacy_charge.get_text() == '* ':
                        legacy_moves.append(' (Legacy)')
                    else:
                        legacy_moves.append('')
                except AttributeError:
                    legacy_moves.append('')

        offensive_grade = soup.find_all(class_=(
            "views-field views-field-field-offensive-moveset-grade"))
        for index, grade in enumerate(offensive_grade):
            offensive_grade[index] = str(grade.get_text().strip())
        defensive_grade = soup.find_all(class_=(
            "views-field views-field-field-defensive-moveset-grade"))
        for index, grade in enumerate(defensive_grade):
            defensive_grade[index] = str(grade.get_text().strip())

        offensive_moves = sorted(zip(offensive_grade[1:], quick[1:],
                                     charge[1:], legacy_moves[1:]),
                                 key=lambda x: x[0])
        defensive_moves = sorted(zip(defensive_grade[1:], quick[1:],
                                     charge[1:], legacy_moves[1:]),
                                 key=lambda x: x[0])

        if len(soup.find_all(class_=("raid-boss-counters"))) > 0:
            raid_counters = soup.find_all(class_=("raid-boss-counters"))[
                0].find_all(class_=("field field--name-title " +
                                    "field--type-string field--label-hidden"))

        title = "%03d" % dex_number + ' | ' + pokemon.upper()
        try:
            descript = "Rating: " + rating[0].get_text().strip() + ' / 5'
        except IndexError:
            descript = "Rating: - / 5"
        if len(types[0].get_text().split()) == 1:
            descript += "\nType: " + types[0].get_text().split()[0]
        else:
            descript += ("\nType: " + types[0].get_text().split()[0] + ' | ' +
                         types[0].get_text().split()[1])
        descript += "\nMax CP: " + max_cp[0].get_text()
        descript += ("\n" + stats[0].get_text().split()[0] + ' ' +
                     stats[0].get_text().split()[1] + ' | ' +
                     stats[1].get_text().split()[0] + ' ' +
                     stats[1].get_text().split()[1] + ' | ' +
                     stats[2].get_text().split()[0] +
                     ' ' + stats[2].get_text().split()[1] + '\n')
        try:
            descript += ("Female: " + female[0].get_text().strip() +
                         " | Male: " + male[0].get_text().strip() + '\n')
        except IndexError:
            pass

        if len(offensive_moves) > 0:

            descript += "\nAttacking Movesets:\n```"
            for (grade, quick, charge, legacy) in offensive_moves:
                descript += ('\n[' + grade.strip() + '] ' + quick.get_text() +
                             ' / ' + charge.get_text() + legacy)
            descript += " \n```"

            descript += "\nDefensive Movesets:\n```"
            for (grade, quick, charge, legacy) in defensive_moves:
                descript += ('\n[' + grade.strip() + '] ' + quick.get_text() +
                             ' / ' + charge.get_text() + legacy)
            descript += "\n```"

            if len(soup.find_all(class_=("raid-boss-counters"))) > 0:

                descript += "\nRaid Boss Counters:\n```"
                for counter in raid_counters:

                    descript += '\n' + counter.get_text()
                descript += "\n```"

        else:

            quick_moves = soup.find(class_=("primary-move")).find_all(class_=(
                "field field--name-title field--type-string " +
                "field--label-hidden"))
            charge_moves = soup.find(class_=("secondary-move")).find_all(
                class_=("field field--name-title field--type-string " +
                        "field--label-hidden"))
            if soup.find(class_=("pokemon-legacy-quick-moves")) is not None:
                quick_legacy = soup.find(class_=(
                    "pokemon-legacy-quick-moves")).find_all(class_=(
                        "field field--name-title field--type-string " +
                        "field--label-hidden"))
            if soup.find(class_=(
                    "secondary-move-legacy secondary-move")) is not None:
                charge_legacy = soup.find(class_=(
                    "secondary-move-legacy secondary-move")).find_all(class_=(
                        "field field--name-title field--type-string " +
                        "field--label-hidden"))

            descript += "\nQuick Moves:\n```"
            for quick_move in quick_moves:
                descript += '\n' + quick_move.get_text()
            if soup.find(class_=("pokemon-legacy-quick-moves")) is not None:
                for legacy_move in quick_legacy:
                    descript += '\n' + legacy_move.get_text() + ' (Legacy)'
            descript += "\n```"

            descript += "\nCharge Moves:\n```"
            for charge_move in charge_moves:
                descript += '\n' + charge_move.get_text()
            if soup.find(class_=(
                    "secondary-move-legacy secondary-move")) is not None:
                for legacy_move in charge_legacy:
                    descript += '\n' + legacy_move.get_text() + ' (Legacy)'
            descript += "\n```"

            if len(soup.find_all(class_=("raid-boss-counters"))) > 0:

                descript += "\nRaid Boss Counters:\n```"
                for counter in raid_counters:

                    descript += '\n' + counter.get_text()
                descript += "\n```"

        em = discord.Embed(title=title, url=site, description=descript,
                           color=Dicts.type_col[
                               types[0].get_text().split()[0].lower()])
        em.set_thumbnail(
            url=(get_image_url("monsters/{:03}_.png")).format(dex_number))

        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
    else:
        em = discord.Embed(
            description=(
                "{} **{}** is not a recognized pokemon, check your spelling."
            ).format(message.author.mention, pokemon.title()),
            color=int('0xee281f', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1


async def set_(bot_number, message):
    msg = message.content.lower().replace('!set ', '').replace(
        '!set\n', '').replace('%', '').replace('nidoranf', 'nidoran♀').replace(
        'nidoranm', 'nidoran♂').replace('mr. mime', 'mr.mime').replace(
        ',\n', ',').replace('\n', ',').replace(', ', ',').split(',')
    set_count = 0
    for command in msg:
        if len(command) == 0:
            continue
        else:
            command = command.strip()
        error = False
        if (get_pkmn_id(command.split()[0].replace(
            'mr.mime', 'mr. mime')) is None or
            command.split()[0] == 'default' or
                command.split()[0] == 'all'):
            pokemon = 'default'
            command = command.replace(pokemon.lower(), '').strip()
            input_ = [command.split()]
            filters = [{
                'min_iv': '0',
                'min_cp': '0',
                'min_level': '0',
                'gender': None,
            }]
        else:
            pokemon = Dicts.locale.get_pokemon_name(get_pkmn_id(
                command.split()[0].replace('mr.mime', 'mr. mime')))
            command = command.replace(
                pokemon.lower().replace(' ', ''), '').strip().split('|')
            if len(command) > 3:
                em = discord.Embed(
                    description=(
                        '{} You can only set a maximum of 3 filters for a ' +
                        'given pokemon.'
                    ).format(message.author.mention),
                    color=int('0xee281f', 16)
                )
                await Dicts.bots[bot_number]['out_queue'].put((
                    1, Dicts.bots[bot_number]['count'], {
                        'destination': message.channel,
                        'embed': em,
                        'timestamp': datetime.utcnow()
                    }
                ))
                Dicts.bots[bot_number]['count'] += 1
                continue
            input_ = []
            filters = []
            for filter_ in command:
                input_.append(filter_.split())
                filters.append({
                    'min_iv': '0',
                    'max_iv': '100',
                    'min_cp': '0',
                    'min_level': '0',
                    'gender': None
                })
        for inp, filt in zip(input_, filters):
            if pokemon != 'default':
                if (len(set(inp).intersection({'female', 'f'})) > 0 and
                    get_pkmn_id(pokemon) not in Dicts.male_only and
                        get_pkmn_id(pokemon) not in Dicts.genderless):
                    filt['gender'] = ['female']
                    filt['ignore_missing'] = True
                    inp.remove(list(set(inp).intersection({'female', 'f'}))[0])
                elif (len(set(inp).intersection({'male', 'm'})) > 0 and
                      get_pkmn_id(pokemon) not in Dicts.female_only and
                      get_pkmn_id(pokemon) not in Dicts.genderless):
                    filt['gender'] = ['male']
                    filt['ignore_missing'] = True
                    inp.remove(list(set(inp).intersection({'male', 'm'}))[0])
                elif (len(set(inp).intersection({'female',
                                                 'f', 'male', 'm'})) > 0):
                    error = True
                    em = discord.Embed(
                        description=(
                            '{} **{}** does not have that gender.'
                        ).format(message.author.mention, pokemon),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
                    break
            for char in inp:
                if is_number(char):
                    if 0 <= int(char) <= 100:
                        filt['min_iv'] = str(char)
                        if int(char) > 0:
                            filt['ignore_missing'] = True
                    else:
                        error = True
                        em = discord.Embed(
                            description=(
                                '{} Pokemon IV must be between 0 and 100.'
                            ).format(message.author.mention),
                            color=int('0xee281f', 16)
                        )
                        await Dicts.bots[bot_number]['out_queue'].put((
                            1, Dicts.bots[bot_number]['count'], {
                                'destination': message.channel,
                                'embed': em,
                                'timestamp': datetime.utcnow()
                            }
                        ))
                        Dicts.bots[bot_number]['count'] += 1
                        break
                elif char.startswith('l') and is_number(char[1:]):
                    if int(char[1:]) >= 1:
                        filt['min_level'] = str(char[1:])
                        if int(char[1:]) > 1:
                            filt['ignore_missing'] = True
                    else:
                        error = True
                        em = discord.Embed(
                            description=(
                                '{} Pokemon level must not be less than 1.'
                            ).format(message.author.mention),
                            color=int('0xee281f', 16)
                        )
                        await Dicts.bots[bot_number]['out_queue'].put((
                            1, Dicts.bots[bot_number]['count'], {
                                'destination': message.channel,
                                'embed': em,
                                'timestamp': datetime.utcnow()
                            }
                        ))
                        Dicts.bots[bot_number]['count'] += 1
                        break
                elif ((char.startswith('cp') or
                       char.endswith('cp')) and
                      is_number(char.replace('cp', ''))):
                    if int(char.replace('cp', '')) >= 10:
                        filt['min_cp'] = str(char.replace('cp', ''))
                        if int(char.replace('cp', '')) > 10:
                            filt['ignore_missing'] = True
                    else:
                        error = True
                        em = discord.Embed(
                            description=(
                                '{} Pokemon CP must not be less than 10.'
                            ).format(message.author.mention),
                            color=int('0xee281f', 16)
                        )
                        await Dicts.bots[bot_number]['out_queue'].put((
                            1, Dicts.bots[bot_number]['count'], {
                                'destination': message.channel,
                                'embed': em,
                                'timestamp': datetime.utcnow()
                            }
                        ))
                        Dicts.bots[bot_number]['count'] += 1
                        break
                elif char.startswith('='):
                    val = char.replace('=', '')
                    if is_number(val):
                        if (0 <= int(char.replace('=', '')) <= 100):
                            filt['min_iv'] = str(char.replace('=', ''))
                            filt['max_iv'] = str(char.replace('=', ''))
                            filt['ignore_missing'] = True
                        else:
                            error = True
                            em = discord.Embed(
                                description=(
                                    '{} Pokemon IV must be between 0 and 100.'
                                ).format(message.author.mention),
                                color=int('0xee281f', 16)
                            )
                            await Dicts.bots[bot_number]['out_queue'].put((
                                1, Dicts.bots[bot_number]['count'], {
                                    'destination': message.channel,
                                    'embed': em,
                                    'timestamp': datetime.utcnow()
                                }
                            ))
                            Dicts.bots[bot_number]['count'] += 1
                            break
                    elif val.startswith('l') and is_number(val[1:]):
                        val = val[1:]
                        if int(val) >= 1:
                            filt['min_level'] = str(val)
                            filt['max_level'] = str(val)
                            filt['ignore_missing'] = True
                        else:
                            error = True
                            em = discord.Embed(
                                description=(
                                    '{} Pokemon level must not be less than 1.'
                                ).format(message.author.mention),
                                color=int('0xee281f', 16)
                            )
                            await Dicts.bots[bot_number]['out_queue'].put((
                                1, Dicts.bots[bot_number]['count'], {
                                    'destination': message.channel,
                                    'embed': em,
                                    'timestamp': datetime.utcnow()
                                }
                            ))
                            Dicts.bots[bot_number]['count'] += 1
                            break
                    elif ((val.startswith('cp') or
                           val.endswith('cp')) and
                          is_number(val.replace('cp', ''))):
                        val = val.replace('cp', '')
                        if int(val) >= 10:
                            filt['min_cp'] = str(val)
                            filt['max_cp'] = str(val)
                            filt['ignore_missing'] = True
                        else:
                            error = True
                            em = discord.Embed(
                                description=(
                                    '{} Pokemon CP must not be less than 10.'
                                ).format(message.author.mention),
                                color=int('0xee281f', 16)
                            )
                            await Dicts.bots[bot_number]['out_queue'].put((
                                1, Dicts.bots[bot_number]['count'], {
                                    'destination': message.channel,
                                    'embed': em,
                                    'timestamp': datetime.utcnow()
                                }
                            ))
                            Dicts.bots[bot_number]['count'] += 1
                            break
                    else:
                        error = True
                        em = discord.Embed(
                            description=(
                                '{} Your command was not recognized ' +
                                '(**{}**).'
                            ).format(message.author.mention, char),
                            color=int('0xee281f', 16)
                        )
                        await Dicts.bots[bot_number]['out_queue'].put((
                            1, Dicts.bots[bot_number]['count'], {
                                'destination': message.channel,
                                'embed': em,
                                'timestamp': datetime.utcnow()
                            }
                        ))
                        Dicts.bots[bot_number]['count'] += 1
                        break
                else:
                    error = True
                    em = discord.Embed(
                        description=(
                            '{} Your command was not recognized ' +
                            '(**{}**).'
                        ).format(message.author.mention, char),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
                    break
            if error is True:
                break
        if error is True:
            continue
        user_dict = Dicts.bots[bot_number]['filters'].get(
            str(message.author.id))
        if user_dict is None:
            Dicts.bots[bot_number]['filters'][str(message.author.id)] = {
                'pokemon': {'enabled': True},
                'eggs': {'enabled': False},
                'raids': {'enabled': False},
                'paused': False
            }
            user_dict = Dicts.bots[bot_number]['filters'][
                str(message.author.id)]
            if args.all_areas is True:
                user_dict['areas'] = Dicts.geofences
            else:
                user_dict['areas'] = []
        if pokemon == 'default':
            user_dict['pokemon'][pokemon] = filters[0]
            for pkmn_id in range(721):
                pkmn = Dicts.locale.get_pokemon_name(pkmn_id + 1)
                if (command.split()[0] == 'all' or
                        pkmn not in user_dict['pokemon']):
                    user_dict['pokemon'][pkmn] = True
        else:
            user_dict['pokemon'][pokemon] = filters
        set_count += 1
    if set_count > 0:
        usr_dict = copy.deepcopy(user_dict)
        Dicts.bots[bot_number]['pokemon_settings'][
            str(message.author.id)] = load_pokemon_section(
                require_and_remove_key('pokemon', usr_dict, 'User command.'))
        Dicts.bots[bot_number]['egg_settings'][
            str(message.author.id)] = load_egg_section(
                require_and_remove_key('eggs', usr_dict, 'User command.'))
        Dicts.bots[bot_number]['raid_settings'][
            str(message.author.id)] = load_pokemon_section(
                require_and_remove_key('raids', usr_dict, 'User command.'))
        update_dicts()
        em = discord.Embed(
            description=(
                '{} You have set **{}** pokemon spawn filters.'
            ).format(message.author.mention, str(set_count)),
            color=int('0x71cd40', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1


async def delete(bot_number, message):
    msg = message.content.lower().replace('!delete ', '').replace(
        '!delete\n', '').replace('!remove ', '').replace(
        '!remove\n', '').replace('%', '').replace(
        'nidoranf', 'nidoran♀').replace('nidoranm', 'nidoran♂').replace(
        'mr. mime', 'mr.mime').replace(',\n', ',').replace('\n', ',').replace(
        ', ', ',').split(',')
    user_dict = Dicts.bots[bot_number]['filters'].get(str(message.author.id))
    if user_dict is None:
        em = discord.Embed(
            description=(
                "{} There is nothing to delete, you don't have any alerts set."
            ).format(message.author.mention),
            color=int('0xee281f', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
    else:
        del_count = 0
        for command in msg:
            if len(command) == 0:
                continue
            else:
                command = command.strip()
            if (command not in ['all', 'default'] and
                    get_pkmn_id(command) is None):
                em = discord.Embed(
                    description=(
                        "{} **{}** is not a recognized pokemon, check your " +
                        "spelling."
                    ).format(message.author.mention, command.title()),
                    color=int('0xee281f', 16)
                )
                await Dicts.bots[bot_number]['out_queue'].put((
                    1, Dicts.bots[bot_number]['count'], {
                        'destination': message.channel,
                        'embed': em,
                        'timestamp': datetime.utcnow()
                    }
                ))
                Dicts.bots[bot_number]['count'] += 1
            elif get_pkmn_id(command) is not None:
                pkmn = Dicts.locale.get_pokemon_name(get_pkmn_id(
                    command.replace('mr.mime', 'mr. mime')))
                if pkmn in user_dict['pokemon']:
                    user_dict['pokemon'].pop(pkmn)
                    del_count += 1
                else:
                    em = discord.Embed(
                        description=(
                            "{} You did not previously have any alerts set " +
                            "for **{}**."
                        ).format(message.author.mention, pkmn),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
            elif command == 'default':
                if 'default' in user_dict['pokemon']:
                    user_dict['pokemon'].pop('default')
                    for pkmn_id in range(721):
                        pkmn = Dicts.locale.get_pokemon_name(pkmn_id + 1)
                        bool = parse_boolean(user_dict['pokemon'].get(pkmn))
                        if bool is True:
                            user_dict['pokemon'].pop(pkmn)
                    del_count += 1
                else:
                    em = discord.Embed(
                        description=(
                            "{} You did not previously have a default set."
                        ).format(message.author.mention),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
            else:
                if len(user_dict['pokemon']) > 1:
                    for filter_ in user_dict['pokemon']:
                        bool = parse_boolean(user_dict['pokemon'][filter_])
                        if bool is not True:
                            del_count += 1
                    user_dict['pokemon'] = {'enabled': True}
                else:
                    em = discord.Embed(
                        description=(
                            "{} You did not previously have any alerts set."
                        ).format(message.author.mention),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
        if (len(user_dict['pokemon']) <= 1 and
            len(user_dict['eggs']) <= 1 and
            len(user_dict['raids']) <= 1 and
            ((len(user_dict['areas']) == 0 and
              args.all_areas is False) or
             (len(user_dict['areas']) == len(Dicts.geofences) and
              args.all_areas is True))):
            Dicts.bots[bot_number]['filters'].pop(str(message.author.id))
            Dicts.bots[bot_number]['pokemon_settings'].pop(
                str(message.author.id))
            Dicts.bots[bot_number]['egg_settings'].pop(str(message.author.id))
            Dicts.bots[bot_number]['raid_settings'].pop(str(message.author.id))
        if del_count > 0:
            if str(message.author.id) in Dicts.bots[bot_number]['filters']:
                usr_dict = copy.deepcopy(user_dict)
                Dicts.bots[bot_number]['pokemon_settings'][
                    str(message.author.id)] = load_pokemon_section(
                        require_and_remove_key(
                            'pokemon', usr_dict, 'User command.'))
                Dicts.bots[bot_number]['egg_settings'][
                    str(message.author.id)] = load_egg_section(
                        require_and_remove_key(
                            'eggs', usr_dict, 'User command.'))
                Dicts.bots[bot_number]['raid_settings'][
                    str(message.author.id)] = load_pokemon_section(
                        require_and_remove_key(
                            'raids', usr_dict, 'User command.'))
            update_dicts()
            em = discord.Embed(
                description=(
                    "{} You have removed **{}** pokemon spawn filters."
                ).format(message.author.mention, str(del_count)),
                color=int('0x71cd40', 16)
            )
            await Dicts.bots[bot_number]['out_queue'].put((
                1, Dicts.bots[bot_number]['count'], {
                    'destination': message.channel,
                    'embed': em,
                    'timestamp': datetime.utcnow()
                }
            ))
            Dicts.bots[bot_number]['count'] += 1


async def reset(bot_number, message):
    msg = message.content.lower().replace('!reset ', '').replace(
        '!reset\n', '').replace('%', '').replace(
        'nidoranf', 'nidoran♀').replace('nidoranm', 'nidoran♂').replace(
        'mr. mime', 'mr.mime').replace(',\n', ',').replace('\n', ',').replace(
        ', ', ',').split(',')
    user_dict = Dicts.bots[bot_number]['filters'].get(str(message.author.id))
    if user_dict is None:
        em = discord.Embed(
            description=(
                "{} There is nothing to reset, you don't have any alerts set."
            ).format(message.author.mention),
            color=int('0xee281f', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
    else:
        reset_count = 0
        for command in msg:
            if len(command) == 0:
                continue
            else:
                command = command.strip()
            if command != 'all' and get_pkmn_id(command) is None:
                em = discord.Embed(
                    description=(
                        "{} **{}** is not a recognized pokemon, check your " +
                        "spelling."
                    ).format(message.author.mention, command.title()),
                    color=int('0xee281f', 16)
                )
                await Dicts.bots[bot_number]['out_queue'].put((
                    1, Dicts.bots[bot_number]['count'], {
                        'destination': message.channel,
                        'embed': em,
                        'timestamp': datetime.utcnow()
                    }
                ))
                Dicts.bots[bot_number]['count'] += 1
            elif get_pkmn_id(command) is not None:
                pkmn = Dicts.locale.get_pokemon_name(get_pkmn_id(
                    command.replace('mr.mime', 'mr. mime')))
                bool = parse_boolean(user_dict['pokemon'].get(pkmn))
                if bool is not True and 'default' in user_dict['pokemon']:
                    user_dict['pokemon'][pkmn] = True
                    reset_count += 1
                elif (pkmn in user_dict['pokemon'] and
                      'default' not in user_dict['pokemon']):
                    user_dict['pokemon'].pop(pkmn)
                    reset_count += 1
                else:
                    em = discord.Embed(
                        description=(
                            '{}, **{}** was already set at your default.'
                        ).format(message.author.mention, pkmn),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
            else:
                if len(user_dict['pokemon']) > 1:
                    for pkmn_id in range(721):
                        pkmn = Dicts.locale.get_pokemon_name(pkmn_id + 1)
                        bool = parse_boolean(user_dict['pokemon'].get(pkmn))
                        if (bool is not True and
                                'default' in user_dict['pokemon']):
                            user_dict['pokemon'][pkmn] = True
                            reset_count += 1
                        elif (pkmn in user_dict['pokemon'] and
                              'default' not in user_dict['pokemon']):
                            user_dict['pokemon'].pop(pkmn)
                            reset_count += 1
                else:
                    em = discord.Embed(
                        description=(
                            "{} You did not previously have any alerts set."
                        ).format(message.author.mention),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
        if (len(user_dict['pokemon']) <= 1 and
            len(user_dict['eggs']) <= 1 and
            len(user_dict['raids']) <= 1 and
            ((len(user_dict['areas']) == 0 and
              args.all_areas is False) or
             (len(user_dict['areas']) == len(Dicts.geofences) and
              args.all_areas is True))):
            Dicts.bots[bot_number]['filters'].pop(str(message.author.id))
            Dicts.bots[bot_number]['pokemon_settings'].pop(
                str(message.author.id))
            Dicts.bots[bot_number]['egg_settings'].pop(str(message.author.id))
            Dicts.bots[bot_number]['raid_settings'].pop(str(message.author.id))
        if reset_count > 0:
            if str(message.author.id) in Dicts.bots[bot_number]['filters']:
                usr_dict = copy.deepcopy(user_dict)
                Dicts.bots[bot_number]['pokemon_settings'][
                    str(message.author.id)] = load_pokemon_section(
                        require_and_remove_key(
                            'pokemon', usr_dict, 'User command.'))
                Dicts.bots[bot_number]['egg_settings'][
                    str(message.author.id)] = load_egg_section(
                        require_and_remove_key(
                            'eggs', usr_dict, 'User command.'))
                Dicts.bots[bot_number]['raid_settings'][
                    str(message.author.id)] = load_pokemon_section(
                        require_and_remove_key(
                            'raids', usr_dict, 'User command.'))
            update_dicts()
            em = discord.Embed(
                description=(
                    "{} You have reset **{}** pokemon spawn filters to your " +
                    "default filter."
                ).format(message.author.mention, str(reset_count)),
                color=int('0x71cd40', 16)
            )
            await Dicts.bots[bot_number]['out_queue'].put((
                1, Dicts.bots[bot_number]['count'], {
                    'destination': message.channel,
                    'embed': em,
                    'timestamp': datetime.utcnow()
                }
            ))
            Dicts.bots[bot_number]['count'] += 1


async def pause(bot_number, author, channel):
    user_dict = Dicts.bots[bot_number]['filters'].get(str(author.id))
    if user_dict is None and channel is not None:
        em = discord.Embed(
            description=(
                "{} There is nothing to pause, you don't have any alerts set."
            ).format(author.mention),
            color=int('0xee281f', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
    elif user_dict['paused'] is True and channel is not None:
        em = discord.Embed(
            description=("{} Your alerts are already paused.").format(
                author.mention),
            color=int('0xee281f', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
    else:
        user_dict['paused'] = True
        update_dicts()
        if channel is not None:
            em = discord.Embed(
                description=("{} Your alerts have been paused.").format(
                    author.mention),
                color=int('0x71cd40', 16)
            )
            await Dicts.bots[bot_number]['out_queue'].put((
                1, Dicts.bots[bot_number]['count'], {
                    'destination': channel,
                    'embed': em,
                    'timestamp': datetime.utcnow()
                }
            ))
            Dicts.bots[bot_number]['count'] += 1


async def resume(bot_number, message):
    user_dict = Dicts.bots[bot_number]['filters'].get(str(message.author.id))
    if user_dict is None:
        em = discord.Embed(
            description=(
                "{} There is nothing to resume, you don't have any alerts set."
            ).format(message.author.mention),
            color=int('0xee281f', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
    elif user_dict['paused'] is False:
        em = discord.Embed(
            description="{} Your alerts were not previously paused.".format(
                message.author.mention),
            color=int('0xee281f', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
    else:
        user_dict['paused'] = False
        update_dicts()
        em = discord.Embed(
            description="{} Your alerts have been resumed.".format(
                message.author.mention),
            color=int('0x71cd40', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1


async def activate(bot_number, message):
    if message.content.lower() == '!activate all':
        msg = Dicts.geofences
    else:
        msg = message.content.lower().replace('!activate ', '').replace(
            '!activate\n', '').replace(',\n', ',').replace('\n', ',').replace(
                ', ', ',').split(',')
    activate_count = 0
    user_dict = Dicts.bots[bot_number]['filters'].get(str(message.author.id))
    for command in msg:
        if len(command) == 0:
            continue
        else:
            command = command.strip()
        if command in Dicts.geofences:
            if user_dict is None:
                if args.all_areas is True:
                    em = discord.Embed(
                        description="{} All areas are on by default.".format(
                            message.author.mention),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
                    break
                else:
                    Dicts.bots[bot_number]['filters'][
                            str(message.author.id)] = {
                        'pokemon': {'enabled': True},
                        'eggs': {'enabled': False},
                        'raids': {'enabled': False},
                        'paused': False,
                        'areas': []
                    }
                    user_dict = Dicts.bots[bot_number]['filters'][
                        str(message.author.id)]
                    usr_dict = copy.deepcopy(user_dict)
                    Dicts.bots[bot_number]['pokemon_settings'][
                        str(message.author.id)] = load_pokemon_section(
                            require_and_remove_key(
                                'pokemon', usr_dict, 'User command.'))
                    Dicts.bots[bot_number]['egg_settings'][
                        str(message.author.id)] = load_egg_section(
                            require_and_remove_key(
                                'eggs', usr_dict, 'User command.'))
                    Dicts.bots[bot_number]['raid_settings'][
                        str(message.author.id)] = load_pokemon_section(
                            require_and_remove_key(
                                'raids', usr_dict, 'User command.'))
                    user_dict['areas'].append(command)
                    activate_count += 1
            elif command not in user_dict['areas']:
                user_dict['areas'].append(command)
                activate_count += 1
            elif message.content.lower() != '!activate all':
                em = discord.Embed(
                    description=(
                        "{} The **{}** area is already active for you."
                    ).format(message.author.mention, command.title()),
                    color=int('0xee281f', 16)
                )
                await Dicts.bots[bot_number]['out_queue'].put((
                    1, Dicts.bots[bot_number]['count'], {
                        'destination': message.channel,
                        'embed': em,
                        'timestamp': datetime.utcnow()
                    }
                ))
                Dicts.bots[bot_number]['count'] += 1
        else:
            em = discord.Embed(
                description=(
                    "{} The **{}** area is an unrecognized area for this " +
                    "region."
                ).format(message.author.mention, command.title()),
                color=int('0xee281f', 16)
            )
            await Dicts.bots[bot_number]['out_queue'].put((
                1, Dicts.bots[bot_number]['count'], {
                    'destination': message.channel,
                    'embed': em,
                    'timestamp': datetime.utcnow()
                }
            ))
            Dicts.bots[bot_number]['count'] += 1
    if (user_dict is not None and
        len(user_dict['pokemon']) <= 1 and
        len(user_dict['eggs']) <= 1 and
        len(user_dict['raids']) <= 1 and
        (len(user_dict['areas']) == len(Dicts.geofences) and
         args.all_areas is True)):
        Dicts.bots[bot_number]['filters'].pop(str(message.author.id))
        Dicts.bots[bot_number]['pokemon_settings'].pop(str(message.author.id))
        Dicts.bots[bot_number]['egg_settings'].pop(str(message.author.id))
        Dicts.bots[bot_number]['raid_settings'].pop(str(message.author.id))
    if activate_count > 0:
        update_dicts()
        em = discord.Embed(
            description=(
                "{} Your alerts have been activated for **{}** areas."
            ).format(message.author.mention, str(activate_count)),
            color=int('0x71cd40', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1


async def deactivate(bot_number, message):
    if message.content.lower() == '!deactivate all':
        msg = Dicts.geofences
    else:
        msg = message.content.lower().replace('!deactivate ', '').replace(
            '!deactivate\n', '').replace(',\n', ',').replace(
                '\n', ',').replace(', ', ',').split(',')
    deactivate_count = 0
    user_dict = Dicts.bots[bot_number]['filters'].get(str(message.author.id))
    for command in msg:
        if len(command) == 0:
            continue
        else:
            command = command.strip()
        if command in Dicts.geofences:
            if user_dict is None:
                if args.all_areas is False:
                    em = discord.Embed(
                        description="{} All areas are off by default.".format(
                            message.author.mention),
                        color=int('0xee281f', 16)
                    )
                    await Dicts.bots[bot_number]['out_queue'].put((
                        1, Dicts.bots[bot_number]['count'], {
                            'destination': message.channel,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
                    break
                else:
                    Dicts.bots[bot_number]['filters'][
                            str(message.author.id)] = {
                        'pokemon': {'enabled': True},
                        'eggs': {'enabled': False},
                        'raids': {'enabled': False},
                        'paused': False,
                        'areas': []
                    }
                    user_dict = Dicts.bots[bot_number]['filters'][
                        str(message.author.id)]
                    usr_dict = copy.deepcopy(user_dict)
                    Dicts.bots[bot_number]['pokemon_settings'][
                        str(message.author.id)] = load_pokemon_section(
                            require_and_remove_key(
                                'pokemon', usr_dict, 'User command.'))
                    Dicts.bots[bot_number]['egg_settings'][
                        str(message.author.id)] = load_egg_section(
                            require_and_remove_key(
                                'eggs', usr_dict, 'User command.'))
                    Dicts.bots[bot_number]['raid_settings'][
                        str(message.author.id)] = load_pokemon_section(
                            require_and_remove_key(
                                'raids', usr_dict, 'User command.'))
                    user_dict['areas'].remove(command)
                    deactivate_count += 1
            elif command in user_dict['areas']:
                user_dict['areas'].remove(command)
                deactivate_count += 1
            elif message.content.lower() != '!deactivate all':
                em = discord.Embed(
                    description=(
                        "{} The **{}** area was not previously active for you."
                    ).format(message.author.mention, command.title()),
                    color=int('0xee281f', 16)
                )
                await Dicts.bots[bot_number]['out_queue'].put((
                    1, Dicts.bots[bot_number]['count'], {
                        'destination': message.channel,
                        'embed': em,
                        'timestamp': datetime.utcnow()
                    }
                ))
                Dicts.bots[bot_number]['count'] += 1
        else:
            em = discord.Embed(
                description=(
                    "{} The **{}** area is an unrecognized area for this " +
                    "region."
                ).format(message.author.mention, command.title()),
                color=int('0xee281f', 16)
            )
            await Dicts.bots[bot_number]['out_queue'].put((
                1, Dicts.bots[bot_number]['count'], {
                    'destination': message.channel,
                    'embed': em,
                    'timestamp': datetime.utcnow()
                }
            ))
            Dicts.bots[bot_number]['count'] += 1
    if (user_dict is not None and
        len(user_dict['pokemon']) <= 1 and
        len(user_dict['eggs']) <= 1 and
        len(user_dict['raids']) <= 1 and
        (len(user_dict['areas']) == 0 and
         args.all_areas is False)):
        Dicts.bots[bot_number]['filters'].pop(str(message.author.id))
        Dicts.bots[bot_number]['pokemon_settings'].pop(str(message.author.id))
        Dicts.bots[bot_number]['egg_settings'].pop(str(message.author.id))
        Dicts.bots[bot_number]['raid_settings'].pop(str(message.author.id))
    if deactivate_count > 0:
        update_dicts()
        em = discord.Embed(
            description=(
                "{} Your alerts have been deactivated for **{}** areas."
            ).format(message.author.mention, str(deactivate_count)),
            color=int('0x71cd40', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1


async def alerts(bot_number, message):
    user_dict = Dicts.bots[bot_number]['filters'].get(str(message.author.id))
    if user_dict is None:
        em = discord.Embed(
            description=(
                "{} You don't have any alerts set."
            ).format(message.author.mention),
            color=int('0xee281f', 16)
        )
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.channel,
                'embed': em,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
    else:
        alerts = "**{}**'s Alert Settings:\nBOT NUMBER: {}\nPAUSED: ".format(
            message.author.mention, str(bot_number + 1))
        if user_dict['paused'] is True:
            alerts += "**TRUE**\n\n"
        else:
            alerts += "**FALSE**\n\n"
        if args.all_areas is True:
            alerts += '__PAUSED AREAS__\n\n```\n'
            if len(user_dict['areas']) == len(
                    Dicts.geofences):
                alerts += 'None\n'
            else:
                for area in list(
                        set(Dicts.geofences) - set(user_dict['areas'])):
                    alerts += '{}, '.format(area.title())
        else:
            alerts += '__ALERT AREAS__\n\n```\n'
            if len(user_dict['areas']) == 0:
                alerts += (
                    "You don't any areas set.  Type `!activate [area/all]` " +
                    "in #filter-commands to set one! \n"
                )
            else:
                for area in user_dict['areas']:
                    alerts += '{}, '.format(area.title())
        alerts = alerts[:-2] + '\n```\n'
        alerts += '__POKEMON__\n\n```\n'
        if 'default' in user_dict['pokemon']:
            alerts += 'Default (all unlisted): '
            filter = user_dict['pokemon']['default']

            if ('max_iv' in filter and
                int(filter['min_iv']) ==
                    int(filter['max_iv'])):
                alerts += '={}%, '.format(
                    filter['min_iv'])
            elif int(filter['min_iv']) > 0:
                alerts += '{}%+, '.format(
                    filter['min_iv'])

            if ('max_cp' in filter and
                int(filter['min_cp']) ==
                    int(filter['max_cp'])):
                alerts += '={}CP, '.format(
                    filter['min_cp'])
            elif int(filter['min_cp']) > 0:
                alerts += '{}CP+, '.format(
                    filter['min_cp'])

            if ('max_level' in filter and
                int(filter['min_level']) ==
                    int(filter['max_level'])):
                alerts += '=L{}, '.format(
                    filter['min_level'])
            elif int(filter['min_level']) > 0:
                alerts += 'L{}+, '.format(
                    filter['min_level'])

            alerts = alerts[:-2] + '\n\n'
        else:
            alerts += 'Default: None\n\n'
        for pkmn_id in range(721):
            pkmn = Dicts.locale.get_pokemon_name(pkmn_id + 1)
            if user_dict['pokemon'].get(pkmn) is True:
                continue
            elif user_dict['pokemon'].get(pkmn) is None:
                if 'default' in user_dict['pokemon']:
                    alerts += '{}: None\n'.format(pkmn.title())
                else:
                    continue
            else:
                alerts += '{}: '.format(pkmn)
                for filter_ in user_dict['pokemon'][pkmn]:
                    if (int(filter_['min_iv']) == 0 and
                        (('max_iv' in filter_ and
                          int(filter_['max_iv']) == 100) or
                            'max_iv' not in filter_) and
                        int(filter_['min_cp']) == 0 and
                        int(filter_['min_level']) == 0 and
                            filter_['gender'] is None):
                        alerts += 'All  '
                    else:
                        if ('max_iv' in filter_ and
                            int(filter_['min_iv']) ==
                                int(filter_['max_iv'])):
                            alerts += '={}%, '.format(filter_['min_iv'])
                        elif int(filter_['min_iv']) > 0:
                            alerts += '{}%+, '.format(filter_['min_iv'])

                        if ('max_cp' in filter_ and
                            int(filter_['min_cp']) ==
                                int(filter_['max_cp'])):
                            alerts += '={}CP, '.format(filter_['min_cp'])
                        elif int(filter_['min_cp']) > 0:
                            alerts += '{}CP+, '.format(filter_['min_cp'])

                        if ('max_level' in filter_ and
                            int(filter_['min_level']) ==
                                int(filter_['max_level'])):
                            alerts += '=L{}, '.format(filter_['min_level'])
                        elif int(filter_['min_level']) > 0:
                            alerts += 'L{}+, '.format(filter_['min_level'])

                        if filter_['gender'] is not None:
                            if filter_['gender'] == ['female']:
                                alerts += '♀, '
                            else:
                                alerts += '♂, '
                    alerts = alerts[:-2] + ' | '
                alerts = alerts[:-3] + '\n'
        alerts += '```'
        alerts = [alerts[i:i+1999] for i in range(0, len(alerts), 1999)]
        for dm in alerts:
            await Dicts.bots[bot_number]['out_queue'].put((
                1, Dicts.bots[bot_number]['count'], {
                    'destination': message.author,
                    'msg': dm,
                    'timestamp': datetime.utcnow()
                }
            ))
            Dicts.bots[bot_number]['count'] += 1


async def areas(bot_number, message):
    user_dict = Dicts.bots[bot_number]['filters'].get(str(message.author.id))
    areas = '__AVAILABLE AREAS__ (Your active areas are in **bold**.)\n\n'
    for area in Dicts.geofences:
        if (user_dict is not None and area in user_dict['areas']):
            areas += '**{}**, '.format(area.title())
        else:
            areas += '{}, '.format(area.title())
    areas = areas[:-2]
    areas += (
        '\n\nYou can change your settings by using `!activate [area/all]` ' +
        'or `!deactivate [area/all]` in #filter-commands'
    )
    areas = [areas[i:i+1999] for i in range(0, len(areas), 1999)]
    for dm in areas:
        await Dicts.bots[bot_number]['out_queue'].put((
            1, Dicts.bots[bot_number]['count'], {
                'destination': message.author,
                'msg': dm,
                'timestamp': datetime.utcnow()
            }
        ))
        Dicts.bots[bot_number]['count'] += 1
